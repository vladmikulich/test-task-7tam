import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Sell from '../views/Sell.vue'
import Host from '../views/Host.vue'
import Support from '../views/Support.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/sell',
    name: 'Sell',
    component: Sell
  },
  {
    path: '/support',
    name: 'Support',
    component: Support
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/host',
    name: 'Host',
    component: Host
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

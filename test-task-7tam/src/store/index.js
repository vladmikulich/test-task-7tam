import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    productsInCart: []
  },
  getters: {
    getProductsInCart (state) {
      return state.productsInCart
    }
  },
  mutations: {
    setProductsInCart (state, payload) {
      state.productsInCart = payload
    },
    addProductToCart (state, payload) {
      state.productsInCart.push(payload)
    }
  },
  actions: {
  },
  modules: {
  }
})
